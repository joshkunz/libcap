############################
##                        ##
##        liblcd          ##
##                        ##
############################

.PHONY: default clean

AR = ar
CC = cc
_unused = -Wno-unused-function -Wno-unused-variable -Wno-unused-parameter
CFLAGS = -I./include -Wall -Wextra -Werror $(_unused)

headers = $(wildcard include/*)
sources = $(wildcard src/*)
objects = $(patsubst %.c,%.o,$(sources))

libdir = ./lib
lib = $(libdir)/libcap.a

default: $(lib) 

%.o: %.c $(headers)
	$(CC) $(CFLAGS) -o $@ -c $<

$(lib): $(objects) 
	@if [[ ! -d $(libdir) ]]; then mkdir $(libdir); fi
	$(AR) -cq $@ $^

clean:
	-rm -rf $(libdir)
	-rm -f $(objects)
